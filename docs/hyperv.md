Usage example for HyperV
========================

### Create a local NAT network for Hyper-V VMs

In Powershell as an Administrator:

```ps1
New-VMSwitch -Name "Local Switch" -SwitchType Internal -Notes "Local switch for fixed network 10.0.0.0/16 (gateway: 10.0.2.2)"
New-NetIPAddress -IPAddress 10.0.2.2 -PrefixLength 16 -InterfaceAlias "vEthernet (Local Switch)"
Get-NetNat | ? Name -Eq "Local Switch Nat" | Remove-NetNat -Confirm:$False
New-NetNat -Name "Local Switch Nat" -InternalIPInterfaceAddressPrefix 10.0.0.0/16
```


## Publish port on the NAT network (example)

Make SSH for VM 10.0.2.10 accessible from the host network on port 2201:

```ps1
Add-NetNatStaticMapping -NatName "Local Switch Nat" -Protocol TCP -ExternalIPAddress 0.0.0.0 -ExternalPort 2210 -InternalIPAddress 10.0.2.10 -InternalPort 22
```

List all port mappings:

```ps1
Get-NetNatStaticMapping
```

Remove a port mapping:

```ps1
Remove-NetNatStaticMapping -StaticMappingID 0
```


### Build an ISO for automated installation (except for IP assignment)

```sh
./debseed -o +local -d -a ip_netmask=255.255.0.0 -a ip_gateway=10.0.2.2 -a username=$USER -a password=test -a "ssh_pubkey=$(cat ~/.ssh/id_rsa.pub)"
```

## References

- https://learn.microsoft.com/en-us/virtualization/hyper-v-on-windows/user-guide/setup-nat-network
