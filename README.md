Debseed
=======

Update Debian ISO images with a [preseed file](https://wiki.debian.org/DebianInstaller/Preseed) to automate Debian installations.

## Usage

This script must be run on Linux with a Bash-compatible shell and requires _xorriso_ and _isolinux_ and optionnally _jinja2_ (to use Jinja2 templates), _whois_ (to use _mkpasswd_), and _curl_, _gnupg_ and _debian-keyring_ (to download and verify Debian official ISO images).

Install all dependencies:

    sudo apt install xorriso isolinux python3-jinja2 whois curl gnupg debian-keyring

When run without any options, check the last available Debian official ISO image, download and verify it if needed and update it with the `preseed.cfg` file in the current working directory.

    ./debseed

See available options:

    ./debseed --help

Examples :

- [HyperV](docs/hyperv.md)


## Available variables in default preseed.cfg and extra files

- `ip_static`: indicate whether to use static IP address configuration, otherwise use DHCP (defauls to `True` if `ip_netmask`, `ip_gateway`, `dns_servers` or `dns_domain` is set, `False` otherwise).
- `ip_netmask` (example: `255.255.0.0`).
- `ip_gateway` (example: `10.0.2.2`).
- `dns_domain` (example: `lan`).
- `dns_servers` (example: `1.1.1.1 8.8.8.8`).
- `ntp_servers` (example: `ntp1.lan ntp2.lan`).
- `hostname`: forced hostname, regardless of what either the DHCP server returns or what the reverse DNS entry for the IP is (example: `debian`).

- `username` (example: `user`).
- `password` (example: `changeme`).
- `password_encrypted`: generated using `mkpasswd -s -m sha-512`, from Debian package `whois` (example: `$6$7MHgCPyLzggV5I3L$8yFgzdCsXxX0Pp0Fn7Tj1NZv1v4a/lIeSQrPhrlo5Qfm/kPRcu6e6s4s2.hGOWSkxNjUTmpFpCLxhnSWOkcb60`).
- `ssh_pubkey` (example: `ssh-rsa AAAAB3NzaC1yc2EA...`).

- `repos_host` (example: `nexus.lan`).
- `repos_base` (example: `/repository`).
- `proxy_url` (example: `http://proxy.lan:3128`).
- `no_proxy` (example: `10.*,192.168.*,172.16.0.0/12,172.16.*,172.17.*,127.*,localhost,*.lan,.lan`)
- `git_url` (example: `https://git.lan`).

- `locale` (example: `fr_FR`, defaults to current system locale).
- `keyboard` (defaults to `fr(latin9)` for locale `fr_FR`).
- `timezone` (defaults to `Europe/Paris` for locale `fr_FR`).
- `local_clock`: indicate whether the hardware clock is in local time (usefull for dual boots with Windows), otherwise it is in UTC (defaults to `False`).

- `auto_part`: perform auto partitionning based on LVM (defaults to `False`).

## Troubleshooting

### Display information about an ISO file

From Debian package `genisoimage`:

    isoinfo -d -i iso/debian...

### Extract initial ramdisk

    cd /tmp/debian...
    mkdir extracted-initrd
    cd extracted-initrd
    sudo cpio -idm < ../initrd
