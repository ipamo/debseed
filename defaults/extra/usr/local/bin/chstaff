#!/bin/sh
#
# Configure `staff` as the group for the given directory and all its current and future sub paths and make them group-writtable.
#
DIR="$1"
if [ $(id -u) -ne 0 ]; then
    echo "chstaff: must be run as root" >&2
    exit 1
fi
if [ ! -e "$DIR" ]; then
    echo "chstaff: path $DIR does not exist" >&2
    exit 1
fi
if [ ! -d "$DIR" ]; then
    echo "chstaff: path $DIR is not a directory" >&2
    exit 1
fi

# Set group for existing files and folder under $DIR
chgrp -R staff "$DIR"

# Set group write permission, and the setgid bit so that files and folder under $DIR will be created with the same group as $DIR
chmod g=rwx,g+s "$DIR"

# Set group ACLs recursively for existing files and folders
setfacl -R -m g::rwx "$DIR"

# Set default group ACLs recursively
setfacl -R -d -m g::rwx "$DIR"
